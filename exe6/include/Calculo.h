#ifndef CALCULO_H
#define CALCULO_H

#include<Maior.h>
#include<Menor.h>
#include<Media.h>

#include <iostream>

using namespace std;

class Calculo
{
    public:
        Calculo();
        Calculo(float v[10]);
        virtual ~Calculo();
        void imprime();

    protected:

    private:
        float valores[10];
        Media media;
        Maior maior;
        Menor menor;
        float mediaNum;
        float maiorNum;
        float menorNum;
};

#endif // CALCULO_H
